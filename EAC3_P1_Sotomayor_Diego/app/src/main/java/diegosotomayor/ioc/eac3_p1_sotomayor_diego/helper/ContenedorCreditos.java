package diegosotomayor.ioc.eac3_p1_sotomayor_diego.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class ContenedorCreditos {

    /**
     * An array of sample (helper) items.
     */
    public static final List<Credito> creditosSemestreActual = new ArrayList<Credito>();

    /**
     * A map of sample (helper) items, by ID.
     */
    public static final Map<String, Credito> ITEM_MAP = new HashMap<String, Credito>();


    static {

        Credito cre = new Credito("M7", "Desarrollo de interfaces", "Un dels camps de la informàtica que es troba en constant evolució i experimentació és el del disseny i " +
                "desenvolupament d’interfícies d’usuari. I això no només es degut a l’evolució d’eines de desenvolupament o aparició de nous estàndards, si no també al desenvolupament d’interfícies hardware que" +
                " donen la volta a la forma d’interacció habitual amb les màquines. ");

        addItem(cre);

        cre = new Credito("M8", "Programación multimédia y de dispositivos móbiles", " Si hi ha un mercat que ha revolucionat el món de la informàtica en els darrers cinc anys, aquest ha estat el de la " +
                "telefonia mòbil i els dispositius portables. Si bé abans els telèfons ja eren programables, aquests eren considerats com unes plataformes minoritàries. Des de l’aparició de l’iPhone (únicament el 2007)" +
                " tot ha sigut un canvi vertiginós on totes les companyies de tecnologia del món (de maquinari i programari) han volgut prendre posició. Tant és així que en aquests anys, a allò que semblava " +
                "impossible se li ha donat un nom: l’era post-PC. ");

        addItem(cre);

        cre = new Credito("M9", "Programación de servicios y procesos", "El cicle formatiu de Desenvolupament d’Aplicacions Multiplataforma compacta tots aquest coneixements en el Mòdul 9 anomenat Programació de serveis i processos. " +
                "El mòdul s’estructura en 3 unitats. La primera unitat que rep el nom de Processos i fils, ofereix una visió del que anomenem programació multifil que garanteixi l’execució de diverses ordres a la vegada treballant amb " +
                "les mateixes dades de forma sincronitzada En aquest mòdul aprendreu tècniques avançades de sincronització i procés paral·lel que us ajudaran a resoldre problemes complexos i de gran volum de forma eficient i robusta. ");

        addItem(cre);


    }

    private static void addItem(Credito item) {
        creditosSemestreActual.add(item);
        ITEM_MAP.put(item.id, item);
    }




    public static class Credito {

        public final String id;
        public final String nombre;
        public final String detalles;

        public Credito(String id, String nombre, String details) {
            this.id = id;
            this.nombre = nombre;
            this.detalles = details;
        }

        @Override
        public String toString() {
            return nombre;
        }
    }
}
